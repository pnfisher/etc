#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  echo "error, you must use sudo to run this script" 1>&2
  exit 1
fi

files=files.lst

if [ ! -f ${files} ]; then
  echo "error, ${files} is missing" 1>&2
  exit 1
fi

while read line; do

  if [[ ! ${line} =~ ^/etc/(.*) ]]; then
    echo "error, $line not prefixed with /etc" 1>&2
    exit 1
  fi

  line=${BASH_REMATCH[1]}

  if [ -d /etc/${line} ]; then
    mkdir -p ${line}
    rsync -a /etc/${line}/ ${line}/
    chown phil -R $(echo ${line} | cut -d '/' -f 1)
    continue
  fi

  if [ -f /etc/${line} ]; then
    mkdir -p $(dirname ${line})
    rsync -a /etc/${line} ${line}
    chown phil -R $(echo ${line} | cut -d '/' -f 1)
    continue
  fi

  echo "error, ${line} is not a file or a directory" 1>&2
  exit 1

done < files.lst
